#!/bin/bash

theme_dir="${HOME}/.config/tint2/whiter"

# Check if tint2 is running
is_running=$(pgrep tint2)
if [ is_running ];then
	# Wait for tint2 to close
	killall tint2 
	while pgrep -u $UID -x sxhkd >/dev/null; do sleep 1; done
fi

# Then launch it
tint2 -c ${theme_dir}/workspaces.tint2rc &
tint2 -c ${theme_dir}/applications.tint2rc &
tint2 -c ${theme_dir}/tray.tint2rc &

# Add a separation between tint2 and applications
tint2 -c ${theme_dir}/vertical-sep.tint2rc &
